
FNAME:=gift2
FLEX_FILE:=src/$(FNAME).l
BISON_FILE:=src/$(FNAME).y

SRC_FILES:=$(wildcard src/*.h)

ifeq "$(DEBUG)" ""
	DEBUG=n
else
	DEBUG=y
endif

ifeq "$(DEBUG)" "y"
	BISON_FLAGS := --graph --debug --verbose --report=state
endif



.PHONY: all

all: BUILD/tokenizer BUILD/gift2html
	@echo "done"

clean:
	@-rm BUILD/*

# For debug purposes
test0: all
	@if [ -f stderr ]; then rm stderr; fi
	BUILD/tokenizer <samples/sample0.gift
#	BUILD/gift2html <samples/sample0.gift 1>stderr 2>&1
	BUILD/gift2html <samples/sample0.gift

test1: all
	@if [ -f stderr ]; then rm stderr; fi
	BUILD/tokenizer <samples/sample1.gift
#	BUILD/gift2html <samples/sample0.gift 1>stderr 2>&1
	BUILD/gift2html <samples/sample1.gift

BUILD/gift2html: $(SRC_FILES) lex.yy.c $(FNAME).tab.c $(FNAME).tab.h
	@echo "building converter"
	$(CXX) -std=c++11 lex.yy.c $(FNAME).tab.c -Isrc/ -lfl -o BUILD/gift2html


BUILD/tokenizer: lex.yy.c $(FNAME).tab.h
	@echo "building tokenizer"
	$(CXX) lex.yy.c -DFLEX_ONLY -lfl -o BUILD/tokenizer

# option -d: debug
lex.yy.c: $(FLEX_FILE) $(BISON_FILE )Makefile
	flex $(FLEX_FILE)

# option -d: generate header for Flex
$(FNAME).tab.h $(FNAME).tab.c: $(SRC_FILES) $(BISON_FILE) Makefile
	bison $(BISON_FLAGS) -d $(BISON_FILE)
#	bison --graph --debug --verbose -d $(BISON_FILE)
