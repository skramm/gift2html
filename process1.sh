#!/bin/bash
set -x

rm *.yy.c
rm parser
rm *tab.*

#bison --verbose --debug -d test1.y
bison -d src/test1.y

flex src/test1.l

g++ lex.yy.c -DFLEX_ONLY -lfl -o parser
./parser <samples/sample1.gift


