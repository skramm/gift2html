#!/bin/bash
set -x

rm *.yy.c
rm parser
rm *tab.*

#bison --verbose --debug -d test1.y
bison -d src/test1.y

flex src/test1.l

g++ -std=c++11 lex.yy.c test1.tab.c -lfl -o parser
./parser <samples/sample1.gift

