// SECTION 1: Prologue
%{
//#define YYDEBUG 1
  #include <cstdio>
  #include <iostream>
  #include <cstdlib>
  #include <vector>
  using namespace std;

  // Declare stuff from Flex that Bison needs to know about:
  extern int yylex();
  extern int yyparse();
  void yyerror(const char *s);

	enum En_QuestionType
	{
		QT_QMC            ///< Multiple choice
		,QT_TF_F
		,QT_TF_T
		,QT_ESSAY         ///< empty answer field
		,QT_SHORT_ANSWER  ///< several good answers
	};
	enum En_QMC
	{
		QMC_GOOD, QMC_BAD
	};
	struct Question
	{
		static size_t   qnum;   ///< question number
		std::string     qtext;  ///< question text
		En_QuestionType qtype;  ///< question type
		std::vector<std::pair<std::string,En_QMC>> v_answers;
		int goodAnswer = -1;   ///< index of good answer, for QMC questions

		void init()
		{
			qtext.clear();
			v_answers.clear();
			goodAnswer = -1;
		}

/// Print the current question, if any
		void purge()
		{
//			std::cout << "PURGE !\n";
			if( !qtext.empty() )
			{
				std::cout << "<p>" << qnum << ": " << qtext << "</p>\n";
			/*	switch( v_answers.size() )
				{
					case 0 :
						qtype = QT_ESSAY;
					break;
					default :
				}*/
				if( v_answers.size() < 2 )
				{
					std::cout << "Error, QMC Question has only one answer!\n";
					init();
					return;
				}
				if( goodAnswer == -1 )
				{
					std::cout << "Error, QMC Question has no good answer!\n";
					init();
					return;
				}

				std::cout << "<ul>\n";
				for( auto i=0; i<(int)v_answers.size(); i++ )
				{
					std::cout << "<li>";
					std::cout << (v_answers[i].second==QMC_GOOD?'G':'F');
					std::cout << ": " <<v_answers[i].first << "</li>\n";
				}
				std::cout << "</ul>\n";
			}
			init();
			qtext.clear();
			v_answers.clear();
			goodAnswer = -1;
		}
		void newQuestion( std::string txt )
		{
			goodAnswer = -1;
			qtext = txt;
			qnum++;
		}
		void addAnswer( En_QMC answer_type, std::string txt )
		{
			v_answers.push_back( std::make_pair(txt,answer_type) );
/*			if( answer_type == QMC_GOOD )
			{
				if( goodAnswer != -1 )
				{
					std::cout << "Error, QMC question has more than one good answer!\n";
					std::exit(1);
				}
				goodAnswer = static_cast<int>( v_answers.size()-1 );
			}*/
		}

	};

	Question g_question; // instanciation of global
%}
// SECTION 2: Bison declarations
%token OPEN
%token CLOSE
%token ELEM
%token STRING
%token BREAK
%%
// SECTION 3: Bison grammar rules
// 'item_list' is a list of items, either one, or one followed by another. May hold none.
item_list:  | item_list item;

// an item has always this structure
item: STRING OPEN element_list CLOSE BREAK
	{
		g_question.purge();
		g_question.newQuestion( (char*)$1 );
	};

// an element_list can be either two elements, or one or another
element_list: | 'T'
	{
//		std::cout << "qTF/T\n";
		g_question.qtype = QT_TF_T;
	}
	| 'F'
	{
//		std::cout << "qTF/T\n";
		g_question.qtype = QT_TF_F;
	}
	| element_list element;

element: '=' STRING
	{
		g_question.addAnswer( QMC_GOOD, (char*)($2) );
	}
	| '~' STRING
	{
		g_question.addAnswer( QMC_BAD, (char*)($2) );
	};
%%
size_t Question::qnum = 0;
int main(int, char**)
{
#ifdef AAAA
// YYDEBUG
  yydebug = 1;
#endif
  // Parse through the input:
  yyparse();
}

void yyerror(const char *s)
{
	cout << "Parse error, message: " << s << endl;
	exit(-1);
}

// SECTION 4:Epilogue

//	g_question.purge();
