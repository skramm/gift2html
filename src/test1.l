
%option yylineno
%{
// all the values are strings
#define YYSTYPE char*
#ifdef FLEX_ONLY
	#define PRINTID(a) std::cout << "-li " << yylineno << ": found '" << a << "'\n"
	#define PRINTID2(a) std::cout << "-li " << yylineno << ": found '" << a << "' item='" << yytext << "'\n"
#else
	#define PRINTID(a) ;
	#define PRINTID2(a) ;
#endif

  #include <iostream>
  #include "gift.tab.h"
  using namespace std;
  extern int yylex();

#ifdef FLEX_ONLY
	YYSTYPE yylval;
#else
	extern YYSTYPE yylval;
#endif
%}
%%
[\n]{2}              { PRINTID("BREAK"); return BREAK; }
[ \t\n]          ; // ignore single line feeds,tabs, ...

"T"			{ PRINTID("T");  return 'T'; }
"F"			{ PRINTID("F");  return 'F'; }
"{"			{ PRINTID("{");  return OPEN; }
"}"			{ PRINTID("}");  return CLOSE; }
"="			{ PRINTID("=");  return '='; /*ELEM; } */ }
"\~"		{ PRINTID("~");  return '~'; /*ELEM; } */ }
\s*[A-Za-z0-9/() ]* {
				yylval= strdup(yytext);
 //	std::cout << " - yytext=" << yytext << "\n - yylval=" << yylval << "\n";
				PRINTID2("string");
				return STRING;
			}
.	        ; // all single characters not matched before
%%
#ifdef FLEX_ONLY
int main(int, char**) {

  // lex through the input:
  while(yylex());
}
#endif

