/**
\file question.hpp
*/
#include <vector>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <cstdlib>

#if 1
	#define TRIGGER( a ) std::cout << "*DEBUG: Trigger rule:" << a << std::endl;
	#define TRIGGER2( a, b ) std::cout << "*DEBUG: Trigger rule:" << a << ", text=" << b << std::endl;
#else
	#define TRIGGER( a )
	#define TRIGGER2( a, b )
#endif // 0

//===================================================================
enum En_QuestionType
{
	QT_QMC            ///< Multiple choice, only one good answer
	,QT_SEVERAL       ///< several good answers
	,QT_TF_F
	,QT_TF_T
	,QT_ESSAY         ///< empty answer field
	,QT_UNIQUE
	,QT_FITB
	,QT_MATCH
};
//===================================================================
enum En_QMC
{
	QMC_GOOD, QMC_BAD
};

//===================================================================
const char* getString( En_QuestionType qt )
{
	const char* n = 0;
	switch( qt )
	{
		case QT_QMC:     n="Multiple Choice"; break;
		case QT_ESSAY:   n="Essay";           break;
		case QT_SEVERAL: n="Several";         break;
		case QT_UNIQUE:  n="Unique";          break;
		case QT_FITB:    n="Fitb";            break;
		case QT_MATCH:   n="Match";           break;
		case QT_TF_F:
		case QT_TF_T:    n="T/F";             break;
		default: assert(0);
	}
	return n;
}

//===================================================================
struct Answer
{
	std::string text;
	En_QMC      type; ///< good or bad answer
	std::string feedback; ///< feedback or string for "match" question
};

//===================================================================
struct Question
{
	static size_t   qnum;   ///< question number
	std::string     qtext;  ///< question text
	std::string     qtext_fitb;  ///< question text second part, for FITB questions
	En_QuestionType qtype;  ///< question type
	En_QMC      _answer_type;
//	std::vector<std::pair<std::string,En_QMC>> v_answers;
	std::vector<Answer> v_answers;

	void init()
	{
		qtext.clear();
		v_answers.clear();
	}

/// Print the current question, if any
	void purge()
	{
		if( qtext.empty() )
		{
			std::cout << "qt EMPTY\n";
			init();
			return;
		}
		if( v_answers.empty() )
		{
			if( qtype != QT_TF_T && qtype != QT_TF_F )
				qtype = QT_ESSAY;
		}
		else
		{
			auto num_good = std::count_if(  // count the number of good & bad answers
				v_answers.begin(),
				v_answers.end(),
				[]                                     // lambda
				(const Answer& a)
				{ return a.type == QMC_GOOD; }
			);
			auto num_bad = v_answers.size() - num_good;

			if( qtype != QT_FITB && qtype != QT_MATCH )
			{
				if( num_bad == 0 )
				{
					if( v_answers.size() > 1 )
						qtype = QT_SEVERAL;
					else
						qtype = QT_UNIQUE;
				}
				else
				{
					if( num_good == 0 )
					{
						std::cout << "Error, Question has no good answer and " << num_bad << " false answer(s)!\n";
						init();
						return;
					}
					qtype = QT_QMC;
				}
			}
		}

		std::cout << "<li><p>" << qnum << ": " << getString(qtype)<< "</p>\n";
		std::cout << "<p>" << qtext;
		if( qtype == QT_FITB )
			std::cout << " _________ " << qtext_fitb;
		std::cout << "?</p>\n";

		if( qtype == QT_TF_F || qtype == QT_TF_T )
		{
			std::cout << "<p>" << (qtype==QT_TF_F?"FALSE":"TRUE" ) << "</p>\n";
		}
		else
		{
			if( qtype != QT_ESSAY && qtype != QT_MATCH )
			{
				std::cout << "<table border='1'>\n";
				std::cout << "<tr><th>G/B</th><th>Answer</th><th>Feedback</th></tr>\n";
				for( auto i=0; i<(int)v_answers.size(); i++ )
				{
					std::cout << "<tr>";
					std::cout << "<td>" << (v_answers[i].type==QMC_GOOD?'G':'F') << "</td>\n";
					std::cout << "<td>" << v_answers[i].text << "</td>\n";
					std::cout << "<td>" << v_answers[i].feedback << "</td>\n";
					std::cout << "</tr>";
				}
				std::cout << "</table>\n";
			}
			if( qtype == QT_MATCH )
			{
				std::cout << "<table border='1'>\n";
				std::cout << "<tr><th>Answer1</th><th>Answer2</th></tr>\n";
				for( auto i=0; i<(int)v_answers.size(); i++ )
				{
					std::cout << "<tr>";
					std::cout << "<td>" << v_answers[i].text << "</td>\n";
					std::cout << "<td>" << v_answers[i].feedback << "</td>\n";
					std::cout << "</tr>";
				}
				std::cout << "</table>\n";
			}
		}
		std::cout << "</li>\n";
		init();
	}
	void setMatch( std::string m1, std::string m2 )
	{
		qtype = QT_MATCH;
		v_answers.push_back( Answer{m1, QMC_GOOD /* irrelevant here */, m2 } );
	}

	void setAnswerText( std::string answer_text, std::string feedback = std::string() )
	{
//		std::cout << __FUNCTION__ << "() at=" << answer_text << " feedback=" << feedback << "\n";
		v_answers.push_back( Answer{answer_text, _answer_type, feedback } );
	}

	void setTrueFalse( bool tf )
	{
		qtype = tf ? QT_TF_T : QT_TF_F;
//		std::cout << __FUNCTION__ << "()" << (qtype==QT_TF_T?"TRUE":"FALSE") << "\n";
	}

	void setAnswerType( En_QMC qmc )
	{
//		std::cout << __FUNCTION__ << "()" << (qmc==QMC_GOOD?"GOOD":"BAD") << "\n";
		_answer_type = qmc;
	}

	void SetFitbText( std::string txt1, std::string txt2 )
	{
		qtext = txt1;
		qtext_fitb = txt2;
		qtype = QT_FITB;
	}
	void SetText( std::string txt, std::string title=std::string() )
	{
		qtext = txt;
		if( !title.empty() )
			std::cout << "TITLE=" << title << "\n";
		qnum++;
	}
#if 0
	void addAnswer( En_QMC answer_type, std::string txt )
	{
		v_answers.push_back( std::make_pair(txt,answer_type) );
//		std::cout << "nb answers=" << v_answers.size() << "\n";
	}
#endif
};

//===================================================================
