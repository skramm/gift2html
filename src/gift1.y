// gift1.y

// SECTION 1: Prologue
%{

#include "question.hpp"

// all values are strings
#define YYSTYPE const char*


// Declare stuff from Flex that Bison needs to know about:
extern int yylex();
extern int yyparse();
void yyerror(const char *s);

	Question g_question; // instanciation of global
%}
//---------------------------------------------------------------
// SECTION 2: Bison declarations
//---------------------------------------------------------------
%token SC_OPEN
%token SC_CLOSE
%token SC_GOOD
%token SC_BAD
%token SC_SHARP
%token SC_MATCH
%token ELEM
%token STRING
%token BREAK
%token COMMENT
%token LF
%token SEP
%token TAG_HTML
%%
//---------------------------------------------------------------
// SECTION 3: Bison grammar rules
//---------------------------------------------------------------

// 'question_list' is a list of questions, either one, or one followed by another. May hold none.
question_list:  | question_list question;

// a question has always this structure
question: COMMENT { TRIGGER( "COMMENT" ); }
	| qheader qanswers SC_CLOSE BREAK
	{
		g_question.purge();
	}
	| qheader qanswers SC_CLOSE STRING BREAK  // FITB (fill in the blank)
	{
		TRIGGER2( "NEW FITB Question", $1 );
		g_question.SetFitbText( $1, $5 );
		g_question.purge();
	};

// header of a question can hold a title or not
qheader: SEP STRING SEP STRING SC_OPEN
	{
		g_question.SetText( $4, $2 );
	}
	| STRING SC_OPEN
	{
		g_question.SetText( $1 );
	};

// a set of answers can be empty, a single (T/F) answer, or hold a set of answers
qanswers: | qanswer | qanswers qanswer
	{ TRIGGER2( "qanswers qanswer", $2 ); };

// rule for answer element
qanswer:
	  special_char_TF                 // TRUE/FALSE
	| special_char_GB STRING                 { g_question.setAnswerText( $2 ); }
	| special_char_GB STRING SC_SHARP STRING { g_question.setAnswerText( $2, $4 ); }
	| special_char_GB STRING SC_MATCH STRING { g_question.setMatch( $2, $4 ); }
	;

special_char_GB:
	  SC_GOOD  { TRIGGER( "GOOD" ); g_question.setAnswerType( QMC_GOOD ); }
	| SC_BAD   { TRIGGER( "BAD" );  g_question.setAnswerType( QMC_BAD  ); }
	;

special_char_TF:
	  'T' { TRIGGER( "TRUE" );  g_question.setTrueFalse( true ); }
	| 'F' { TRIGGER( "FALSE" ); g_question.setTrueFalse( false ); }
	;

/*qanswer: SC_GOOD STRING
	{
		TRIGGER2( "GA", $2 );
		g_question.addAnswer( QMC_GOOD, $2 );
	}
	| SC_BAD STRING
	{
		TRIGGER2( "BA", $2);
		g_question.addAnswer( QMC_BAD, $2 );
	}
	| 'T' { TRIGGER( "TRUE" );  g_question.setTrueFalse( true ); }
	| 'F' { TRIGGER( "FALSE" ); g_question.setTrueFalse( false ); }
	;
*/
%%
size_t Question::qnum = 0;
int main(int, char**)
{
#if YYDEBUG==1
	std::cout << "-Build with YYDEBUG defined\n";
	yydebug = 1;
#endif

	std::cout << "<html><head>\n";
	std::cout << "</head><body>\n";
	std::cout << "<ol>\n";

	// Parse through the input:
	yyparse();
	std::cout << "</ol>\n";
	std::cout << "</body></html>\n";
}

void yyerror(const char *s)
{
	std::cout << "Parse error, message: " << s << '\n';
}

// SECTION 4:Epilogue

