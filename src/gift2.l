

%option yylineno
%{
// all the values are strings
//#define YYSTYPE char*
#define YYSTYPE std::string

#ifdef FLEX_ONLY
	#define PRINTID(a) std::cout << "-li " << yylineno << ": found '" << a << "'\n"
	#define PRINTID2(a) std::cout << "-li " << yylineno << ": found '" << a << "' item='" << yytext << "'\n"
#else
	#define PRINTID(a) ;
	#define PRINTID2(a) ;
#endif

  #include <iostream>
  #include "gift2.tab.h"
  using namespace std;
  int yylex();

#ifdef FLEX_ONLY
	YYSTYPE yylval;
#else
	extern YYSTYPE yylval;
#endif
%}
%%
\/\/.*\n             { PRINTID("Comment"); return COMMENT; }
[\n]{2}              { PRINTID("BREAK"); return BREAK; }
[\n]              { PRINTID("LINEFEED"); return LF; }
[:]{2}              { PRINTID("Sep"); return SEP; }
"->"              { PRINTID("MATCH"); return SC_MATCH; }

[ \t]          ; // ignore single line feeds,tabs, ...
"[html]"    { PRINTID("tag:html"); return TAG_HTML; }
"T"			{ PRINTID("T"); return 'T'; }
"F"			{ PRINTID("F"); return 'F'; }
"{"			{ PRINTID("{"); return SC_OPEN;  }
"}"			{ PRINTID("}"); return SC_CLOSE; }
"="			{ PRINTID("="); return SC_GOOD;  }
"\~"		{ PRINTID("~"); return SC_BAD;   }
"#"			{ PRINTID("~"); return SC_SHARP; }
\s*[A-Za-z0-9\/()\+\- ]* {
				yylval= strdup(yytext);
 //	std::cout << " - yytext=" << yytext << "\n - yylval=" << yylval << "\n";
				PRINTID2("string");
				return STRING;
			}
.	        ; // all single characters not matched before
%%
#ifdef FLEX_ONLY
int main(int, char**)
{
  while(yylex())
	;
}
#endif

