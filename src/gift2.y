// gift2.y

// SECTION 1: Prologue
%{

#include "question.hpp"

// all values are strings
//#define YYSTYPE const char*
#define YYSTYPE std::string


// Declare stuff from Flex that Bison needs to know about:
extern int yylex();
extern int yyparse();
void yyerror(const char *s);

	Question g_question; // instanciation of global
%}
//---------------------------------------------------------------
// SECTION 2: Bison declarations
//---------------------------------------------------------------
%token SC_OPEN
%token SC_CLOSE
%token SC_GOOD
%token SC_BAD
%token SC_SHARP
%token SC_MATCH
%token ELEM
%token STRING
%token BREAK
%token LF
%token COMMENT
%token SEP
%token TAG_HTML
%%
//---------------------------------------------------------------
// SECTION 3: Bison grammar rules
//---------------------------------------------------------------

// general rule: a string on different contiguous lines is a single string
full_string: STRING LF  { TRIGGER2( "string !", $1 ); };
	| STRING LF STRING LF { $$ = $1 + " " + $3;  TRIGGER2( "string concat", $$ ); };

question_list:  | question_list question;

question:
//COMMENT { TRIGGER( "COMMENT" ); }
//	|
	qheader qanswers SC_CLOSE BREAK
	{
		g_question.purge();
	}
	;

// header of a question can hold a title (single line) or not
qheader:
	| full_string SC_OPEN
	{
		g_question.SetText( $1 );
	};

// a set of answers can be empty, a single (T/F) answer, or hold a set of answers
qanswers: | qanswer | qanswers qanswer
	{ TRIGGER2( "qanswers qanswer", $2 ); };

// rule for answer element
qanswer:
	| special_char_GB full_string  { TRIGGER2( "qanswer", $2 ); g_question.setAnswerText( $2 ); }
	;

special_char_GB:
	  SC_GOOD  { TRIGGER( "GOOD" ); g_question.setAnswerType( QMC_GOOD ); }
	| SC_BAD   { TRIGGER( "BAD" );  g_question.setAnswerType( QMC_BAD  ); }
	;


%%
size_t Question::qnum = 0;
int main(int, char**)
{
#if YYDEBUG==1
	std::cout << "-Build with YYDEBUG defined\n";
	yydebug = 1;
#endif
	// Parse through the input:
	yyparse();
}

void yyerror(const char *s)
{
	std::cout << "Parse error, message: " << s << '\n';
//	std::exit(-1);
}

// SECTION 4:Epilogue

