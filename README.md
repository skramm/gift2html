# gift2html

Gift to Html converter

STATUS: PRE-ALPHA (do not try to use this...)

CURRENTLY NO USABLE (this project is in "standby" state)


The idea here is to do something like this:
https://fuhrmanator.github.io/GIFT-grammar-PEG.js/docs/editor/editor.html
but locally

Reference: https://docs.moodle.org/36/en/GIFT_format


## 1 - Build instructions

### Required tools
 - Flex + Bison
 - C++11 compiler

Ref:
 - Flex: https://ftp.gnu.org/old-gnu/Manuals/flex-2.5.4/html_mono/flex.html
 - Bison https://www.gnu.org/software/bison/manual/bison.html

 ### Instruction

 $ make

## 2 - Running instruction

After building, you have a binary called gift2html

usage: `./gift2html <inputfile.gift >index.html`

## Misc.

Gift format Special characters: ~ = # { }

## status:
 - handles different types of questions
 - does not handle multiline questions

